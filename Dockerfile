FROM node:carbon
#1. DOWNLOAD CERTIFICATE APM
RUN apt-get update &&  apt-get install -y --no-install-recommends
#2. INSTALL LIBRERIA DE DEPENDENCIAS
RUN apt-get install -y alien libaio1 \
    -y apt-utils unzip telnet  \
    git build-essential python  
#3. CLEAN ARCHIVOS TEMPORALES
RUN apt-get clean  \ 
    && rm -rf /var/lib/apt/lists/* 
#4. CREA DIRECTORIOS WORK DIR
RUN mkdir -p /app/dummycronjob
RUN mkdir -p /app/dummycronjob/log
WORKDIR /app/dummycronjob
#5. COPYA PACKATE JSON E INSTALLA DEPENDENCIAS NODE.JS
COPY package.json /app/dummycronjob/
RUN npm install
#6. COPYA ARCHIVOS A CARPETAS WORKDIR
COPY . /app/dummycronjob
ENV NODE_ENV ${ENVIRONMENT}
#7. EXPONE SERVICIO PUERTO 3000
EXPOSE 3000
#ENV NODE_ENV=production
CMD [ "npm", "start" ]

